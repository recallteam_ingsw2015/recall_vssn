# reCall's VSSN #

Questa è la repository del **VSSN** creato dal gruppo **reCall**, formato da Riccardo Affolter, Francesco Galasso e Simone Matteucci.  

Importando il progetto nel proprio IDE, il nome della cartella sarà **VSSN_reCall**.  

Tutta la documentazione, è disponibile nella sezione wiki.