package it.recall.vssn.server;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import it.recall.vssn.server.ReCallServiceImpl;
import it.recall.vssn.shared.Utente;
import it.recall.vssn.shared.Msg;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/*
 * !!! ATTENZIONE !!!
 * Per eseguire il test correttamente e con successo, assicurarsi di
 * non avere file di database di testing salvati nella cartella /db/ .
 * Nel caso fossero presenti i file db , db.p e db.t al suo interno,
 * è necessario rimuoverli. NON rimuovere la cartella /db/ altrimenti
 * verrà prodotto un errore, in quanto MapDB non riesce a generare la
 * "cartella parent" attraverso la sua chiamata.
 * La cartella /war/db/ non è da toccare e non è riferita al test, ma
 * all'esecuzione "normale" dell'applicazione.
 */

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReCallServiceServerImplTest {

	private ReCallServiceImpl originalServer;
	private ReCallServiceImpl testServer;

	@Before
	public void prepare() {
		originalServer = new ReCallServiceImpl();
		testServer = Mockito.spy(originalServer);
	}
	@Test
	public void a_insertNewUser() {

		try{
			Utente u = new Utente("JUnit Test User", "New York", "12-03-1990", 'M', "simone", "1234", "testing@example.com");
			ArrayList<String> inserted = testServer.insertUser(u);
				if (inserted.get(0).equals("false"))
					fail("Utente gia' esistente nel database o errore nel db");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo insertNewUser() : " + e.getMessage());
		}
	}
	
	@Test
	public void b_insertExistingUser() {
		try{
			Utente u = new Utente("JUnit Test User", "New York", "12-03-1990", 'M', "simone", "1234", "testing@example.com");
			ArrayList<String> inserted = testServer.insertUser(u);
				if (inserted.get(0).equals("true"))
					fail("Inserito utente gia' esistente nel database o errore nel db");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo insertExistingUser() : " + e.getMessage());
		}
	}
	
	@Test
	public void c_loginWrong() {
		try{
			ArrayList<String> reply = testServer.login("simone","4321");
			String response = reply.get(0);
				if (response!="passwdWrong")
					fail("Effettuato login con credenziali errate");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo loginError() : " + e.getMessage());
		}
	}
	
	@Test
	public void d_loginOk() {
		try{
			ArrayList<String> reply = testServer.login("simone","1234");
			String response = reply.get(0);
				if (response!="loginOk")
					fail("Login non effettuato correttamente");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo loginOk() : " + e.getMessage());
		}
	}
	
	@Test
	public void e_addFollowing(){
		Utente u = new Utente("JUnit Test User", "Modena", "01-01-1990", 'M', "riccardo", "1234", "testing@example.com");
		testServer.insertUser(u);
		testServer.addFollowing("simone", u.getUsername());
		try{
		if(!u.checkFollowerList("simone"))
			fail("L'utente non ha follower o errore nel db");
		}catch(Exception e){
		fail("Eccezione eseguendo il metodo addFollowing() : "+ e.getMessage());
			}
	}
	
	@Test
	public void f_removeFollowing(){
		try{
			testServer.removeFollowing("simone", "riccardo");
			}catch(Exception e){
			fail("Eccezione eseguendo il metodo removeFolowing() : "+ e.getMessage());
			}
	}
	
	@Test
	public void g_searching(){
		try{
			ArrayList<String> search = testServer.searching("s", "riccardo");
			if (search.isEmpty())
				fail("Utente non presente nei follower o errore nel db");
		}catch(Exception e){
			fail("Eccezione eseguendo il metodo searching() : " + e.getMessage());
		}
	}
	
	@Test
	public void h_retrieveFollowingUsers(){
		try{
		ArrayList<String> reply = testServer.retrieveFollowingUsers("riccardo");
		if(!reply.isEmpty())
			fail("L'utente ha follower o errore nel db");
		}catch(Exception e){
		fail("Eccezione eseguendo il metodo retrieveFollowingUsers() : "+ e.getMessage());
			}
	}
	
	@Test
	public void i_manageAdministrator(){
		Utente admin = new Utente(null, null, null, 'm', "admin", "password");
		try{
			testServer.manageAdministrator();
			ArrayList<String> inserted = testServer.insertUser(admin);
			if (inserted.get(0).equals("true"))
				fail("Utente Admin non inserito nel database o errore nel db");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo manageAdministrator() : " + e.getMessage());
		}
	}
	
	@Test
	public void l_insertMessage() {
		try{
		String reply = testServer.insertMessage("Televisione", "Simone", "Amo Un posto al sole");
			if (reply.equals("ERROR"))
				fail("Errore nel salvataggio del messaggio!");
		}
		catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void m_fillMessage() {

		try{
		ArrayList<Msg> reply = testServer.fillMessageBoard("","");
			if (reply.isEmpty())
				fail("Lista messaggi vuota");
		}
		catch(Exception e){
			fail(e.getMessage());
		}
	} 
	
	@Test
	public void n_deleteMessage() {
		try{
		boolean test = testServer.deleteMessage(1);
			if (!test)
				fail("Errore nell'eliminazione del messaggio!");
		}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo deleteMessage() : " + e.getMessage());
		}
	}
	
	
	@Test
	public void o_retrieveCategories() {

		try{
			HashMap<String, String> categoriesList = testServer.retrieveCategories();
				if (categoriesList.containsKey("RETRIEVE_CATEGORIES__ERROR"))
					fail("Errore nella fase di recupero lista categorie del db - EXCEPTION " + categoriesList.get("RETRIEVE_CATEGORIES__ERROR"));
		}
		catch(Exception e){
			fail("Eccezione testando il metodo retrieveUsers() : " + e.getMessage());
		}
	}


	@Test
	public void p_insertCategory(){
		try{
		boolean test = testServer.insertCategory("TestingCategory");
		if (!test)
			fail("Categoria non inserita correttamente nel database o errore nel db");
	}
		catch(Exception e){
			fail("Eccezione eseguendo il metodo insertCategory() : " + e.getMessage());
		}
	}
	@Test
	public void q_deleteCategory(){
		try{
			boolean test = testServer.deleteCategory("TestingCategory");
			if (!test)
				fail("La categoria non � stata eliminata correttamente nel database o errore nel db");
		}
			catch(Exception e){
				fail("Eccezione eseguendo il metodo insertCategory() : " + e.getMessage());
			}
	}

}
