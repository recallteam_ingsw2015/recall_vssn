package it.recall.vssn.server;

import it.recall.vssn.client.service.ReCallService;
import it.recall.vssn.shared.KeyMsg;
import it.recall.vssn.shared.Msg;
import it.recall.vssn.shared.Utente;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.Atomic;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class ReCallServiceImpl extends RemoteServiceServlet implements ReCallService {

	DB db = DBMaker.newFileDB(new File("db/db")).closeOnJvmShutdown().encryptionEnable("password").make();
    protected ConcurrentNavigableMap<KeyMsg,Msg> messaggi;
    protected ConcurrentNavigableMap<String,Utente> utenti;
    protected ConcurrentNavigableMap<String,String> listaCategorieDb;
    
    // Inserisce un utente nel database se lo username scelto non è
    // già stato usato da qualcun altro.
    // Nel database viene inserito come chiave lo username dell'utente
    // e come valore l'oggetto utente.
	@Override
	public ArrayList<String> insertUser(Utente ut) {
		
		utenti = db.getTreeMap("usersList");
		ArrayList<String> result = new ArrayList<>();

		if (!utenti.containsKey(ut.getUsername())) {
			utenti.put(ut.getUsername(), ut);
			db.commit();
			result.add("true");
			result.add(ut.getUsername());
			return result;
		} else {
			db.commit();
			result.add("false");
			return result;
		}

	}

	// Inserisce un nuovo messaggio nel database ottenendo come parametri
	// la categoria del messaggio, lo username del mittente e il messaggio stesso.
	// Il metodo si occupa inoltre di salvare anche il timestamp dell'inserimento
	// del messaggio che verrà visualizzato e l'id univoco del messaggio.
	// Il database salva come chiave un oggetto contenente la categoria, il mittente
	// e l'id del messaggio, come valore un oggetto contenente i valori di cui sopra,
	// il messaggio e il timestamp del messaggio.
	public String insertMessage(String categoria, String utente, String msg) {
		String replyMsg = null;
		try {
			messaggi = db.getTreeMap("msgList");

			Atomic.Integer keyIncr = db.getAtomicInteger("usersKey");
			Integer cnt = keyIncr.incrementAndGet();
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = new Date();

			messaggi.put(new KeyMsg(categoria, utente, cnt), new Msg(msg,
					categoria, utente, "Inviato il " + dateFormat.format(date) + " alle " + timeFormat.format(date),cnt));

			db.commit();

			replyMsg = "messaggio salvato nel database!";
		} catch (Exception e) {
			replyMsg = "ERROR";
		}
		return replyMsg;
	}

	// Ottengo dal database la lista di messaggi che hanno come categoria
	// e come nome utente quelli passati come parametri. Se passati parametri vuoti,
	// si intendono come "non selezionati" e quindi non vengono filtrati secondo quel filtro.
	@Override
	public ArrayList<Msg> fillMessageBoard(String user, String category) {
		
		messaggi = db.getTreeMap("msgList");
		
		ArrayList<Msg> list = new ArrayList<Msg>();
		
		if(user.equals("") && category.equals("")){
			for (KeyMsg key : messaggi.keySet()) {
				list.add(messaggi.get(key));
			}
		}
		else if(!user.equals("") && category.equals("")){
			for (KeyMsg key : messaggi.keySet()) {
				if(key.getMittente().equals(user)){
					list.add(messaggi.get(key));
				}
			}
		}
		else if(user.equals("") && !category.equals("")){
			for (KeyMsg key : messaggi.keySet()) {
				if(key.getCategoria().equals(category)){
					list.add(messaggi.get(key));
				}
			}
		}
		else if(!user.equals("") && !category.equals("")){
			for (KeyMsg key : messaggi.keySet()) {
				if(key.getMittente().equals(user) && key.getCategoria().equals(category)){
					list.add(messaggi.get(key));
				}
			}
		}

		return list;
	}

	// Restituisce la lista di following dell'utente passato come parametro.
	@Override
	public ArrayList<String> retrieveFollowingUsers(String usernameLogged) {
		try {
		 utenti = db.getTreeMap("usersList");
		 
		 Utente ut = utenti.get(usernameLogged);
	    
	    ArrayList<String> listaUtentiSeguiti = new ArrayList<>();
    
	    for(String s : ut.getFollowerList())
	    	listaUtentiSeguiti.add(s);
	    
	    return listaUtentiSeguiti;
		} catch (NullPointerException e){
			ArrayList<String> errore = new ArrayList<>();
			errore.add("@@ERRORE@@");
			return errore;
		}
	    
	}
	
	// Restituisce la lista di categorie presenti nel db.
	@Override
	public HashMap<String, String> retrieveCategories() {
		listaCategorieDb = db.getTreeMap("categoriesList");
	    
	    HashMap<String, String> listaCategorie = new HashMap<>();
	    
	    for(Entry e : listaCategorieDb.entrySet()){
	    	listaCategorie.put(e.getKey().toString(),e.getValue().toString());
	    }
	    
	    return listaCategorie;
	    
	}
	
	/**
	 * Metodo richiamato da ClientImpl che controlla se sulla NavigableMap e' presente l'utente ricercato con quella password e ritorna un 
	 * ArrayList per la gestione lato client dell'errore(LoginCallback).
	 */
	@Override
	public ArrayList<String> login(String username,String password) {

		utenti = db.getTreeMap("usersList");
	    
	    ArrayList<String> infoLogin = new ArrayList<String>(); 
	    
	    if(!username.equals("") && !password.equals("")){
	    if(utenti.containsKey(username))
	    {
	    	if(utenti.get(username).getPassword().equals(password)){
	    		infoLogin.add("loginOk");
	    		infoLogin.add(username);
	    	}
	    	else
	    		infoLogin.add("passwdWrong");
	    }
	    else
	    	infoLogin.add("usernameWrong");
	    }
	    else
	    	infoLogin.add("missing");

		return infoLogin;
	}
	
		/**
		 * Ricerca sull'utente loggato(cookie presente) di tutti gli utenti presenti che assolvano l'espressione regolare 
		 * presente nel for each ,interrogando la Map "utenti". Ritorna al client un ArrayList in cui presento si gli utenti -u(unfollow)
		 * che gli utenti -f(follow).
		 */
		public ArrayList<String> searching(String searching, String usernameLogged) {
			
		utenti = db.getTreeMap("usersList");
		Utente ut = utenti.get(usernameLogged);

		ArrayList<String> listOfSearched = new ArrayList<String>();

		for (String s : utenti.keySet()) {
			if (!searching.equals("") && s.matches("(?i)"+searching+".*")) {
				String userSearched = utenti.get(s).getUsername();
				if(ut.checkFollowerList(userSearched)){ 
					String reply = userSearched +"-u";
					listOfSearched.add(reply);
				}
				else{
					String reply = userSearched +"-f";
					listOfSearched.add(reply);
				}
			}
		}
		
		return listOfSearched;
	}
	
	// Aggiunge il following ad un utente nell'oggetto utente desiderato
	// presente nel db
	public void addFollowing(String username2Follow, String username){
		utenti = db.getTreeMap("usersList");
		Utente ut = utenti.get(username);
		
		ut.addFollowingList(username2Follow);	//aggiungo following
		utenti.put(ut.getUsername(), ut);		//aggiorno info Utente -> possibile solo con un PUT

		db.commit();
	}
	
	// Rimuove il following ad un utente nell'oggetto utente desiderato
	// presente nel db
	public void removeFollowing(String username2Follow, String username){
		utenti = db.getTreeMap("usersList");
		Utente ut = utenti.get(username);
		
		ut.removeFollowingList(username2Follow);	//rimuovo following
		utenti.put(ut.getUsername(), ut);			//aggiorno info Utente -> possibile solo con un PUT

		db.commit();
	}
	
	// Viene aggiunto l'utente administrator al db
	public void manageAdministrator() {
		utenti = db.getTreeMap("usersList");

		if (!utenti.containsKey("admin")) {
			Utente admin = new Utente(null, null, null, 'm', "admin", "admin");
			utenti.put(admin.getUsername(), admin);
		}
	}
	
	// Viene inserita una categoria nel db se la categoria passata
	// come parametro non è già esistente
	public boolean insertCategory(String newCategory){
		listaCategorieDb = db.getTreeMap("categoriesList");
		boolean insert = false;
	    
		if(!listaCategorieDb.containsKey(newCategory)){
			listaCategorieDb.put(newCategory,newCategory);
			insert = true;
		}
	   
		db.commit();
		return insert;
	}
	
	// Elimina un messaggio dal db con l'id passato come parametro
	public boolean deleteMessage(int cnt){
		boolean deleted = false;
		messaggi = db.getTreeMap("msgList");
		
		for (KeyMsg key : messaggi.keySet()) {
			if(key.getId().equals(cnt)){
				messaggi.remove(key);
				deleted = true;
			}
		}
		
		db.commit();
		
		return deleted;
		
	}
	
	// Viene eliminata la categoria passata come parametro
	// se non sono presenti messaggi in quella categoria.
	public boolean deleteCategory(String delCategory){
		listaCategorieDb = db.getTreeMap("categoriesList");
		messaggi = db.getTreeMap("msgList");
		
		ArrayList<Msg> list = new ArrayList<Msg>();
		
		boolean delete = false;
	    
		for (KeyMsg key : messaggi.keySet()){
			if(key.getCategoria().equals(delCategory))
				list.add(messaggi.get(key));
		}
		
		if(list.isEmpty()){
			listaCategorieDb.remove(delCategory);
			delete=true;
		}
	   
		db.commit();
		return delete;

	}
}
