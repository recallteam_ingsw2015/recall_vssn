package it.recall.vssn.client;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class VSSN_reCall implements EntryPoint {
	
	/**
	 * This is the entry point method.
	 * All'avvio vengono caricati i seguenti moduli/widget, inizializzati e caricati dalla classe
	 * dell'implementazione del client.
	 * Vengono quindi caricati tutti widget (componenti grafici) che andranno ad arricchire
	 * l'interfaccia utente.
	 */
	public void onModuleLoad() {
		
		// Collego le classi utilizzabili dal client al server all'indirizzo specificato.
		ReCallClientImpl impl = new ReCallClientImpl(GWT.getModuleBaseURL() + "vssn_recall");
		
		// Se nel db non è presente l'utente amministratore, verrà creato.
		impl.manageAdministrator();
		
		// Carico le componenti della GUI.
		RootPanel.get("navbar").add(impl.getGUINavbar());
		RootPanel.get("insertMsg").add(impl.getInsertMsgGUI());
		RootPanel.get("filtro").add(impl.getFiltersGUI());
		RootPanel.get("sideMenu").add(impl.getSideMenuGUI());
		RootPanel.get("msgBoard").add(impl.getMsgBoardGUI());
		RootPanel.get("AdminP").add(impl.getAdminPanelGui());
		
	}
	
}
