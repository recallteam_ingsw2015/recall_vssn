package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;
import it.recall.vssn.shared.Msg;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class MessageBoard extends Composite {

	// Riferimento all'oggetto creato con uiBinder
	private static MessageBoardUiBinder uiBinder = GWT.create(MessageBoardUiBinder.class);
	
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField VerticalPanel msgBoard;

	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;

	interface MessageBoardUiBinder extends UiBinder<Widget, MessageBoard> {
	}

	// Costruttore della classe, setta delle proprietà grafiche in html.
	public MessageBoard(ReCallClientImpl reCallClientImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		msgBoard.getElement().addClassName("msgBoard");
		this.serviceImpl = reCallClientImpl;
	}

	// Genera un oggetto messaggio (un singolo messaggio da visualizzare,
	// e lo inserisce all'interno della message board.
	public void addMsgToMsgBoard(Msg s, Boolean showFollowLabel, Boolean showDeleteMsgLabel) {
		final Message msg = new Message(serviceImpl);
		this.msgBoard.setBorderWidth(1);

		msg.setAuthorLbl(s.getUser());
		msg.setCategoryLbl(s.getCategory());
		msg.setTextMsg(s.getText());
		msg.setTimestamp(s.getTimestamp());
		msg.setDeleteLabel(s.getId());
		msg.showDeleteMsgLabel(showDeleteMsgLabel);

		this.msgBoard.add(msg);
	}
	
	// Pulisce la message board
	public void cleanMsgBoard(){
		msgBoard.clear();
	}
	
}
