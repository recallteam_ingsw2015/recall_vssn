package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class User extends Composite {
	// Riferimento all'oggetto creato con uiBinder
	private static FollowerUiBinder uiBinder = GWT.create(FollowerUiBinder.class);
	// Riferimenti ai componenti presenti nell'interfaccia(label e button)
	@UiField Label infoFollowerLbl;
	@UiField Button followBtn;
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;
	// Riferimento all'utente loggato
	String usernameLogged;
	
	interface FollowerUiBinder extends UiBinder<Widget, User> {
	}
	// Costruttore della classe, setta le proprietÓ grafiche e riconosce l'utente loggato
	public User(ReCallClientImpl reCallClientImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		usernameLogged = Cookies.getCookie("VSSN_usernameLogged");
		followBtn.getElement().setClassName("btn btn-default");
		this.serviceImpl = reCallClientImpl;
	}
	//a seconda del risultato della ricerca setta la visibilitÓ della label e il risultato
	public void setInfoUser(String info){
		if(!info.equals("NESSUN RISULTATO TROVATO!")){
			infoFollowerLbl.setText(info);
			followBtn.setVisible(true);
		}
		else{
			infoFollowerLbl.setText(info);
			followBtn.setVisible(false);
		}
	}
	//mostra lo stato dell'utente(follow/unfollow)
	public void showStatusFollowing(String status){
		if(!status.equals("f"))
			followBtn.setText("Unfollow");
		else
			followBtn.setText("Follow");
	}
	//all'evento del clik inserisce o rimuove (follow/unfollow) username2Follow a l'utente loggato 	
	@UiHandler("followBtn")
	void onFollowBtnClick(ClickEvent event) {
		String username2Follow = infoFollowerLbl.getText();

		if(followBtn.getText().equals("Follow")){
			serviceImpl.addFollowing(username2Follow, usernameLogged);
			followBtn.setText("Unfollow");
		}
		else{
			serviceImpl.removeFollowing(username2Follow, usernameLogged);
			followBtn.setText("Follow");
		}
	}
}
