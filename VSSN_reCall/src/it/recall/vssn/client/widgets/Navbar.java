package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;
import it.recall.vssn.client.widgets.extendedobjects.UlListPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class Navbar extends Composite {

	// Riferimento all'oggetto creato con uiBinder
	private static NavbarUiBinder uiBinder = GWT.create(NavbarUiBinder.class);
	
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField HTMLPanel container;
	@UiField HTMLPanel containerIN;
	@UiField HTMLPanel containerBtn;
	@UiField Label welcome;
	
	// Oggetto che riferisce ad un elenco puntato necessario alla costruzione della navbar
	UlListPanel ul;
	
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;

	interface NavbarUiBinder extends UiBinder<Widget, Navbar> {
	}
	
	// Costruttore della classe
	public Navbar(ReCallClientImpl serviceImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		this.serviceImpl = serviceImpl;
	}

	public Navbar() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	// Creo i bottoni da inserire nella navbar
	private void loadButtons(boolean showLogin, boolean showLogout, boolean showRegistrati){
		ul = new UlListPanel();
		ul.setStyleName("nav navbar-nav navbar-right");
		
		if(showLogin){
			// Creo anchor login
			Anchor login = new Anchor();
			login.setText("Login");
			login.setStyleName("clickableNoLink");
			
			// Creo click handler per anchor login e aggiungo il widget
			ul.addWithClickHandler(login,new ClickHandler(){

				@Override
				public void onClick(ClickEvent event) {
					ReCallClientImpl urImpl = serviceImpl;
					Login dialog = urImpl.getGUILogin();
					dialog.cleanLogin();
					dialog.center();
			        dialog.show();
				}
				
			});
		}
		
		if(showRegistrati){
			// Creo anchor registrati
			Anchor registrati = new Anchor();
			registrati.setText("Registrati");
			registrati.setStyleName("clickableNoLink");
			
			// Creo click handler per anchor logout e aggiungo il widget
			ul.addWithClickHandler(registrati,new ClickHandler(){

				@Override
				public void onClick(ClickEvent event) {
					ReCallClientImpl urImpl = serviceImpl;
					Registrazione dialog = urImpl.getGUIRegistrazione();
					dialog.center();
			        dialog.show();
				}
				
			});
		}
		
		if(showLogout){
			
			// Mostro utente loggato nella navbar
			Anchor hello = new Anchor();
			hello.setText("Ciao, " + Cookies.getCookie("VSSN_usernameLogged"));
			ul.add(hello);
			
			// Creo anchor logout
			Anchor logout = new Anchor();
			logout.setText("Logout");
			logout.setStyleName("clickableNoLink");
			
			// Creo click handler per anchor logout e aggiungo il widget
			ul.addWithClickHandler(logout,new ClickHandler(){

				@Override
				public void onClick(ClickEvent event) {
					serviceImpl.removeLoginCookie();
				}
				
			});
			
		}
		
		containerBtn.add(ul);
	}
	
	// Pulisco i bottoni presenti in precedenza nella navbar e ricarico i bottoni desiderati
	public void isLogged(){
		containerBtn.clear();
		loadButtons(false, true, false);
		containerBtn.add(ul);
		welcome.setVisible(true);
	}
	
	// Pulisco i bottoni presenti in precedenza nella navbar e ricarico i bottoni desiderati
	public void isNotLogged(){
		containerBtn.clear();
		loadButtons(true, false, true);
		containerBtn.add(ul);
		welcome.setVisible(false);
	}

}
