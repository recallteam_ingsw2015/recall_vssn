package it.recall.vssn.client.widgets;

import java.util.Date;

import it.recall.vssn.client.service.ReCallClientImpl;
import it.recall.vssn.shared.Utente;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.Label;

public class Registrazione extends DialogBox {

	// Riferimento all'oggetto creato con uiBinder
	private static RegistrazioneUiBinder uiBinder = GWT.create(RegistrazioneUiBinder.class);
	
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField Button registratiButton;
	@UiField TextBox username;
	@UiField TextBox password;
	@UiField TextBox nome;
	@UiField TextBox citta;
	@UiField RadioButton sessoM;
	@UiField RadioButton sessoF;
	@UiField TextBox email;
	@UiField TextBox dataNascita;
	@UiField Label labelok;
	@UiField Button chiudiButton;
	
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;

	interface RegistrazioneUiBinder extends UiBinder<Widget, Registrazione> {
	}

	// Costruttore della classe, setta delle proprietà grafiche in gwt e in html.
	public Registrazione(ReCallClientImpl serviceImpl) {
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setWidget(uiBinder.createAndBindUi(this));
		this.serviceImpl = serviceImpl;
		sessoM.setValue(true);
		username.getElement().setAttribute("placeholder", "Username");
		password.getElement().setAttribute("placeholder", "Password");
		nome.getElement().setAttribute("placeholder", "Nome");
		citta.getElement().setAttribute("placeholder", "Citta");
		email.getElement().setAttribute("placeholder", "Email");
		dataNascita.getElement().setAttribute("placeholder", "Data di Nascita (gg-mm-aaaa)");
	}
	
	public Registrazione() {
		}

	// Setta un messaggio che viene mostrato nella label labelok.
	public void setLabelok(String text, String color){
		if(color.equals("nero")){
			labelok.getElement().getStyle().setColor("black");
			labelok.setText(text);
		}
		if(color.equals("rosso")){
			labelok.getElement().getStyle().setColor("red");
			labelok.setText(text);
		}
	}

	// Creo l'utente (e il relativo oggetto) direttamente a livello client e lo invio al server
	@UiHandler("registratiButton")
	void onRegistratiButtonClick(ClickEvent event) {
		registrazioneAction(username.getText(), password.getText(), nome.getText(), citta.getText(), dataNascita.getText(), email.getText(), sessoM.getValue(), sessoF.getValue());
	}
	
	// Nasconde il pannello
	@UiHandler("chiudiButton")
	void onChiudiButtonClick(ClickEvent event) {
		hideMe();	
	}
	
	// Nasconde la GUI del modulo registrazione
	public void hideMe(){
		this.hide();
	}
	
	// Metodo per la registrazione di un utente
	private void registrazioneAction(String username, String password, String nome, String citta, String dataNascita, String email, Boolean sessoM, Boolean sessoF){
		// Controllo se tutti i campi obbligatori sono stati riempiti...
		if(!username.equals("") &&
		   !password.equals("") &&
		   !nome.equals("") &&
		   !citta.equals("") &&
		   !dataNascita.equals("")){
			// verifico la validita' dello username (solo caratteri alfanumerici)
			if(verificaUsername(username)){
				// verifico la validita' della data di nascita inserita
				if(verificaData(dataNascita)){
					// ...se e' non e' stata inserita una mail:
					if(email==""){
						if(sessoM==true){
							Utente ut = new Utente(nome, citta, dataNascita, 'M', username, password);	
							serviceImpl.insertUser(ut);
						}
						if(sessoF==true){
							Utente ut = new Utente(nome, citta, dataNascita, 'F', username, password);
							serviceImpl.insertUser(ut);
						}
					}
					// ...se e' stata inserita una mail:
					else{
						if(verificaEmail(email)){
							if(sessoM==true){
								Utente ut = new Utente(nome, citta, dataNascita, 'M', username, password, email);
								serviceImpl.insertUser(ut);
							}
							if(sessoF==true){
								Utente ut = new Utente(nome, citta, dataNascita, 'F', username, password, email);
								serviceImpl.insertUser(ut);
							}
						}
						else{
							labelok.getElement().getStyle().setColor("red");
							labelok.setText("L'indirizzo email inserito non è valido");
						}
					}
				}
				// se data nascita non valida
				else {
					labelok.getElement().getStyle().setColor("red");
					labelok.setText("La data di nascita inserita non è valida (dd-mm-aaaa)");
				}
			}
			// se username con caratteri non alfanumerici
			else {
				labelok.getElement().getStyle().setColor("red");
				labelok.setText("Solo caratteri alfanumerici nello username");
			}
		}
		// Altrimenti visualizzo messaggio di errore
		else{
			labelok.getElement().getStyle().setColor("red");
			labelok.setText("Riempire tutti i campi obbligatori");
		}
	}
	
	// Verifico la validita' data inserita
	private boolean verificaData(String data){
		String regex = "([0-9]{2})-([0-9]{2})-([0-9]{4})";
		RegExp pattern = RegExp.compile(regex);
		MatchResult result = pattern.exec(data);
		if(result != null){
			DateTimeFormat dtf = DateTimeFormat.getFormat("dd-MM-yyyy");
			try {
				Date dataValida = dtf.parseStrict(data);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		
		return (result != null);
	}
	
	// Verifico la validita' della sintassi dell'email inserita
	private boolean verificaEmail(String email){
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";;
		RegExp pattern = RegExp.compile(regex);
		MatchResult result = pattern.exec(email);
		return (result != null);
	}
	
	// Verifico la validita' della sintassi dello username inserito
	private boolean verificaUsername(String username){
		String regex = "^[A-Za-z0-9]";
		RegExp pattern = RegExp.compile(regex);
		MatchResult result = pattern.exec(username);
		return (result != null);
	}
	
	// Pulisce il modulo di registrazione
	public void cleanModule(){
		username.setText("");
		password.setText("");
		nome.setText("");
		citta.setText("");
		email.setText("");
		dataNascita.setText("");
		sessoM.setValue(true);
		sessoF.setValue(false);
		labelok.setText("");
	}
	
}
