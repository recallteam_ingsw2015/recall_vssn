package it.recall.vssn.client.widgets.extendedobjects;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Node;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.Widget;

public class UlListPanel extends ComplexPanel implements InsertPanel {

	public UlListPanel() {
		setElement(Document.get().createULElement());
	}

	private LiPanel wrapWidget(Widget w) {
		LiPanel li = new LiPanel();
		li.add(w);
		return li;
	}
	
	private LiPanel wrapWidgetWithClickHandler(Widget w, ClickHandler c) {
		LiPanel li = new LiPanel();
		li.add(w);
		li.sinkEvents(Event.ONCLICK);
		li.addHandler(c, ClickEvent.getType());
		return li;
	}

	@Override
	public void add(Widget w) {
		add(wrapWidget(w), getElement());
	}
	
	public void addWithClickHandler(Widget w, ClickHandler c){
		add(wrapWidgetWithClickHandler(w,c), getElement());
	}

	@Override
	public void clear() {
		try {
			
		} finally {
			Node child = getElement().getFirstChild();
			while (child != null) {
				getElement().removeChild(child);
				child = getElement().getFirstChild();
			}
		}
	}

	public void insert(Widget w, int beforeIndex) {
		insert(wrapWidget(w), getElement(), beforeIndex, true);
	}

	private static class LiPanel extends ComplexPanel implements InsertPanel {

		protected LiPanel() {
			setElement(Document.get().createLIElement());
		}
		
		@Override
		public void add(Widget w) {
			add(w, getElement());
		}

		@Override
		public void clear() {
			try {
				
			} finally {
				Node child = getElement().getFirstChild();
				while (child != null) {
					getElement().removeChild(child);
					child = getElement().getFirstChild();
				}
			}
		}

		public void insert(Widget w, int beforeIndex) {
			insert(w, getElement(), beforeIndex, true);
		}
	}
}