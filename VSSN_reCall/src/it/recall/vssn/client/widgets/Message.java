package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class Message extends Composite {

	// Riferimento all'oggetto creato con uiBinder
	private static MessageUiBinder uiBinder = GWT.create(MessageUiBinder.class);
	
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField Label authorLbl;
	@UiField Label categoryLbl;
	@UiField Label textLbl;
	@UiField Label timestamp;
	@UiField Label utenteFixedLabel;
	@UiField HorizontalPanel horizontalUtente;
	@UiField HTMLPanel boxMessaggio;
	@UiField Label deleteMsgLabel;
	
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;

	interface MessageUiBinder extends UiBinder<Widget, Message> {
	}

	// Costruttore della classe, setta delle proprietà grafiche in html.
	public Message(ReCallClientImpl reCallClientImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		
		/* Settando vari stili css agli elementi.... */
		this.utenteFixedLabel.getElement().setAttribute("style", "padding-right: 4px;");
		this.authorLbl.getElement().setAttribute("style", "font-weight: bold;");
		this.horizontalUtente.getElement().setAttribute("style", "float: left;");
		this.categoryLbl.getElement().setAttribute("style", "float: right; padding-left:2px; padding-right:2px;background-color: rgba(0, 156, 255, 0.3);");
		this.timestamp.getElement().addClassName("timestamp");
		this.deleteMsgLabel.getElement().addClassName("followLabel");
		this.deleteMsgLabel.getElement().setAttribute("style", "color: #E81515;");
		this.boxMessaggio.getElement().setAttribute("align", "center");
		this.textLbl.getElement().setAttribute("align", "left");
		this.timestamp.getElement().setAttribute("align", "left");
		
		/* Collegamento all'implementazione lato client */
		this.serviceImpl = reCallClientImpl;
	}
	
	// Inserisce l'autore del messaggio nella casella messaggio
	public void setAuthorLbl(String author){
		authorLbl.setText(author);
	}
	
	// Inserisce la categoria del messaggio nella casella messaggio
	public void setCategoryLbl(String category){
		categoryLbl.setText(category);
	}

	// Inserisce il messaggio nella casella messaggio
	public void setTextMsg(String msg){
		textLbl.setText(msg);
	}
	
	// Inserisce il timestamp del messaggio nella casella messaggio
	public void setTimestamp(String timestamp){
		this.timestamp.setText(timestamp);
	}
	
	// Inserisce il pulsante per l'eliminazione del messaggio nella casella messaggio
	public void setDeleteLabel(final int cnt){
		/* ClickHandler per le label follow e unfollow dei messaggi*/
		deleteMsgLabel.addClickHandler(new ClickHandler() {
		    @Override
		    public void onClick (ClickEvent event){
		    	serviceImpl.deleteMessage(cnt);
		    }
		});
	}
	
	// Mostra o meno il pulsante per l'eliminazione del messaggio
	public void showDeleteMsgLabel(boolean show){
		this.deleteMsgLabel.setVisible(show);
	}
	
}
