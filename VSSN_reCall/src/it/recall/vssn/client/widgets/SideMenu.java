package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;

public class SideMenu extends Composite {

	// Riferimento all'oggetto creato con uiBinder
	private static SideMenuUiBinder uiBinder = GWT.create(SideMenuUiBinder.class);
	
	// Riferimenti ai componenti presenti nell'interfaccia
	@UiField TextBox searchingText;
	@UiField Button searchingButton;
	@UiField VerticalPanel searchingResultPanel;
	@UiField VerticalPanel followingListPanel;
	@UiField HTMLPanel sideMenuContainer;
	@UiField HorizontalPanel searchInputPanel;
	
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;
	
	String usernameLogged;

	interface SideMenuUiBinder extends UiBinder<Widget, SideMenu> {
	}

	public SideMenu() {
	}
	
	// Costruttore della classe, setta delle proprietà grafiche in gwt e in html.
	public SideMenu(ReCallClientImpl serviceImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		usernameLogged = Cookies.getCookie("VSSN_usernameLogged");
		this.serviceImpl = serviceImpl;
		searchInputPanel.getElement().setAttribute("style", "margin-bottom:5px;");
		searchingButton.getElement().setAttribute("style", "margin-left:5px;");
		searchingButton.getElement().setAttribute("class", "form-control btn btn-default");
		searchingText.getElement().setAttribute("class", "form-control");
		searchingText.getElement().setAttribute("placeholder", "Cerca un utente");
	}
	
	// Cerco l'username del Client all'interno del database
	@UiHandler("searchingButton")
	void onSearchingButtonClick(ClickEvent event) {
		serviceImpl.searching(searchingText.getText(), usernameLogged);
		searchingText.setText("");
	}
	
	// Viene mostrato l'oggetto User, contentente l'username dell'utente ricercato
	// ed un pulsate sul quale è visibile lo status dell'utente ricercato:
	// follow se non è ancora stato aggiunto alla propria followingList e
	// unfollow se è già presente
	public void showSearchResults(String msg){
		final User user = new User(serviceImpl);
		
		String[] parts = msg.split("-");
		String showName = parts[0];
		String status = parts[1];
		
		user.setInfoUser(showName);
		user.showStatusFollowing(status);
		searchingResultPanel.add(user);
	}
	
	// Viene mostrata la lista dei propri Following all'inteno dell'area dedicata
	// dentro il SideMenu
	public void showFollowing(String userFollowing){
		final User user = new User(serviceImpl);
		user.setInfoUser(userFollowing);
		user.showStatusFollowing("u");
		followingListPanel.add(user);
	}
	
	// Ripulisco la sezione della ricerca
	public void cleanResultPanel(){
		searchingResultPanel.clear();
	}
	
	// Ripulisco la sezione della visualizzazione dei propri following
	public void cleanFollowingListPanel(){
		followingListPanel.clear();
	}
	
	// viene settato l'username dell'utente loggato, in modo da essere sempre disponibile
	public void setUsernameLogged(String username){
		this.usernameLogged=username;
	}
	
	// Viene settata una proprietà css all'oggetto SideMenu
	public void showSideMenu(){
		this.sideMenuContainer.getElement().setClassName("radius");
	}
	
}
