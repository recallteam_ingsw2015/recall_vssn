package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class AdminPanel extends Composite{
	// Riferimento all'oggetto creato con uiBinder
	private static AdminPanelUiBinder uiBinder = GWT.create(AdminPanelUiBinder.class);
	// Riferimenti ai componenti presenti nell'interfaccia
	@UiField HTMLPanel AdminPanel;
	@UiField Button deleteCat;
	@UiField TextBox insertCatTxtBox;
	@UiField Button insertButtonAdmin;
	@UiField ListBox listCatAdmin;
	@UiField Label adminLbl;
	@UiField Label adminLbl2;
	@UiField VerticalPanel addCategoriesVPanel;
	@UiField VerticalPanel manageCategoriesVPanel;
	@UiField HorizontalPanel addCategoriesHPanel;
	@UiField HorizontalPanel manageCategoriesHPanel;
	// Riferimento all'implementazione del client
	ReCallClientImpl serviceImpl;

	interface AdminPanelUiBinder extends UiBinder<Widget, AdminPanel> {
	}

	public AdminPanel() {
	}


	public AdminPanel(ReCallClientImpl reCallClientImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		this.serviceImpl = reCallClientImpl;
		/* Settando graficamente gli elementi nel panel... */
		addCategoriesVPanel.getElement().setAttribute("style", "margin-bottom: 10px;");
		addCategoriesVPanel.getElement().setAttribute("align", "center");
		manageCategoriesVPanel.getElement().setAttribute("style", "margin-bottom: 10px;");
		manageCategoriesVPanel.getElement().setAttribute("align", "center");
		deleteCat.getElement().setAttribute("style", "margin-left: 5px");
		insertButtonAdmin.getElement().setAttribute("style", "margin-left: 5px");
		insertCatTxtBox.getElement().setAttribute("placeholder", "Nome Categoria");
		addCategoriesHPanel.getElement().setAttribute("style", "margin-top: 5px;");
		manageCategoriesHPanel.getElement().setAttribute("style", "margin-top: 5px;");
	}
	//restituisce il risultato dell'inserimento o no della categoria(se gi� esistente)
	public void showResultOperationAdmin(boolean b){
		if(b)
			adminLbl.setText("Categoria inserita");
		else
			adminLbl.setText("Categoria già esistente");
	}
	//inserimento della gategoria con la correzione di errori da input(categoria vuota)
	@UiHandler("insertButtonAdmin")
	void onInsertButtonAdminClick(ClickEvent event) {
		String cat = insertCatTxtBox.getText();
		if(!cat.isEmpty()){
			serviceImpl.insertCategory(cat);
			insertCatTxtBox.setText("");
		}
		else
			adminLbl.setText("Impossibile aggiunge Categoria vuota");
	}
	//elimino la categoria
	@UiHandler("deleteCat")
	void onDeleteClick(ClickEvent event) {
		int index = listCatAdmin.getSelectedIndex();
		serviceImpl.deleteCategory(listCatAdmin.getItemText(index));
	}
	
	public void cleanAdminPanel(){
		AdminPanel.clear();
	}
	
	public void clean(){
		listCatAdmin.clear();
	}
	
	public void addCategoryToListAdmin(String category){
		this.listCatAdmin.addItem(category);
	}
	
	// operazione lato cliente dell'utente admin (insert/remove category)
	public void showResultInsertionOperationAdmin(boolean b){
		adminLbl.getElement().setAttribute("style", "color:#23527C; text-align:center;");
		if(b)
			adminLbl.setText("Aggiunta nuova categoria");
		else
			adminLbl.setText("Categoria già presente");
	}
	
	public void showResultDeleteOperationAdmin(boolean b){
		adminLbl2.getElement().setAttribute("style", "color:#23527C; text-align:center;");
		if(b)
			adminLbl2.setText("Categoria cancellata");
		else
			adminLbl2.setText("Errore : Categoria selezionata non vuota");
	}
	
	 // metodi di manipolazione grafica dell'output o di pulizia
	 
	public void showAdminPanel(){
		this.AdminPanel.getElement().setAttribute("style", "padding-top:10px; padding-bottom:10px; background-color: rgba(178, 225, 255, 0.5);");
		this.AdminPanel.getElement().setClassName("radius");
	}
	
	public void cleanLabels(){
		this.adminLbl.setText("");
		this.adminLbl2.setText("");
	}
}
