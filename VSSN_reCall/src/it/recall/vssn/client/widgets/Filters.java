package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class Filters extends Composite {

	// Riferimento all'oggetto creato con uiBinder
	private static FiltersUiBinder uiBinder = GWT.create(FiltersUiBinder.class);
	
	// Liste (grafiche) di categorie e utenti filtrabili
	@UiField ListBox categorie;
	@UiField public ListBox utentiFollowing;
	
	// Categoria e utente selezionati
	String catSelez, utSelez;
	
	// Riferimento all'implementazione del client
	ReCallClientImpl serviceImpl;

	interface FiltersUiBinder extends UiBinder<Widget, Filters> {
	}

	public Filters() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	// Costruttore della classe, inizializza le variabili catSelez e utSelez
	// e setta delle proprietà grafiche in html.
	public Filters(ReCallClientImpl serviceImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		catSelez = "";
		utSelez = "";
		categorie.getElement().setClassName("form-control");
		categorie.getElement().setAttribute("style", "display: inline !important; width: 250px !important;");
		utentiFollowing.getElement().setClassName("form-control");
		utentiFollowing.getElement().setAttribute("style", "display: inline !important; width: 250px !important;");
		this.serviceImpl = serviceImpl;
	}
	
	// Aggiunge un utente alla lista di following visualizzati
	public void addUserToList(String user){
		this.utentiFollowing.addItem(user);
	}

	// Salva nella variabile utSelez l'utente selezionato
	private void addToUtSel(int index){
		this.utSelez = utentiFollowing.getItemText(index);
	}
	
	// Aggiunge una categoria alla lista di following visualizzati 
	public void addCategoryToList(String category, String description){
		this.categorie.addItem(category);
	}
	
	// Salva nella variabile catSelez l'utente selezionato
	private void addToCatSel(int index){
		this.catSelez = categorie.getItemText(index);
	}
	
	// Mostra un messaggio e disabilita la lista di following
	public void setUtentiFilterLabel(String message, String status){
		if(status.equals("disabled")){
			this.utentiFollowing.setEnabled(false);
			this.utentiFollowing.getElement().setAttribute("id", "disabledInput");
		}
		this.utentiFollowing.addItem(message);
	}
	
	// Mostra un messaggio e disabilita la lista di categorie
	public void setCategorieFilterLabel(String message, String status){
		if(status.equals("disabled")){
			this.categorie.setEnabled(false);
			this.categorie.getElement().setAttribute("id", "disabledInput");
		}
		this.categorie.addItem(message);
	}
	
	// Quando viene selezionata una categoria, viene salvata la scelta in locale,
	// e anche nella classe dell'implementazione del client
	@UiHandler("categorie")
	void catSelClick(ClickEvent event){
		addToCatSel(categorie.getSelectedIndex());
		if(!categorie.getItemText(categorie.getSelectedIndex()).equals("Tutte le categorie")){
			serviceImpl.filterCategory(catSelez);
		}
		else{
			serviceImpl.filterCategory("");
		}
	}
	
	// Quando viene selezionato un utente, viene salvata la scelta in locale,
	// e anche nella classe dell'implementazione del client
	@UiHandler("utentiFollowing")
	void utSelClick(ClickEvent event){
		addToUtSel(utentiFollowing.getSelectedIndex());
		if(!utentiFollowing.getItemText(utentiFollowing.getSelectedIndex()).equals("Tutti gli utenti")){
			serviceImpl.filterUser(utSelez);
		}
		else{
			serviceImpl.filterUser("");
		}
	}
	
	// Pulisco la lista di following
	public void cleanLstBoxFollowing(){
		utentiFollowing.clear();
	}
	
	// Pulisco la lista di categorie
	public void cleanLstBoxCategory(){
		categorie.clear();
	}
	
	// Abilito la lista di following
	public void enable(){
		this.utentiFollowing.getElement().setAttribute("id", "");
		this.utentiFollowing.getElement().removeAttribute("disabled");
	}
	
	// Abilito la lista di categorie
	public void enableCategorie(){
		this.categorie.getElement().setAttribute("id", "");
		this.categorie.getElement().removeAttribute("disabled");
	}
	
}
