package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class Login extends DialogBox {
	private ReCallClientImpl serviceImpl;
	// Riferimento all'oggetto creato con uiBinder
	private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField TextBox username;
	@UiField PasswordTextBox password;
	@UiField Label labelok;
	@UiField Button chiudiButton;
	
	interface LoginUiBinder extends UiBinder<Widget, Login> {
	}
	/**
	 * @param serviceImpl
	 * Viene inizializzata la DialogBox con la possibilita' di inserire username e password.
	 */
	public Login(ReCallClientImpl serviceImpl) {
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setWidget(uiBinder.createAndBindUi(this));
		this.serviceImpl = serviceImpl;
		username.getElement().setAttribute("placeholder", "Username");
		password.getElement().setAttribute("placeholder", "Password");
	}
	public Login() {
		setText("Login");
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setWidget(uiBinder.createAndBindUi(this));
	}
	/**
	 * @param event viene richiamato dato l'evento ClickEvent il metodo di login dell'oggetto serviceImpl per la chiamata di verifica sul server
	 */
	@UiHandler("loginButton")
	void onLoginButtonClick(ClickEvent event) {
		serviceImpl.login(username.getText(), password.getText());
	}
	
	@UiHandler("chiudiButton")
	void onChiudiButtonClick(ClickEvent event) {
		this.hide();
	}
	//restituisce l'username dell'utente
	public String getUsername() {
		return username.getValue();
	}
	
	public void setLabelok(String text){
		labelok.getElement().setAttribute("style", "color:red");
		labelok.setText(text);
	}

	public Label getLabelok() {
		return labelok;
	}
	public TextBox getUser() {
		return username;
	}
	public PasswordTextBox getPass() {
		return password;
	}
	//pulisce il dialogBox
	public void cleanLogin(){
		username.setText("");
		password.setText("");
		labelok.setText("");
	}
}
