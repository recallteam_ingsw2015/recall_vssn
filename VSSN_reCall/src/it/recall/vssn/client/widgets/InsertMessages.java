package it.recall.vssn.client.widgets;

import it.recall.vssn.client.service.ReCallClientImpl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

public class InsertMessages extends Composite {
	
	// Riferimenti ai componenti presenti nell'interfaccia grafica
	@UiField Button inviaMsgButton;
	@UiField TextArea testoMsg;
	@UiField Label labelok;
	@UiField ListBox categoryListBox;
	@UiField HTMLPanel boxMessage;
	
	// Riferimento all'implementazione del client
	private ReCallClientImpl serviceImpl;

	// Riferimento all'oggetto creato con uiBinder
	private static InsertMessagesUiBinder uiBinder = GWT.create(InsertMessagesUiBinder.class);

	interface InsertMessagesUiBinder extends UiBinder<Widget, InsertMessages> {
	}

	// Costruttore della classe, setta delle proprietà grafiche in html.
	public InsertMessages(ReCallClientImpl reCallClientImpl) {
		initWidget(uiBinder.createAndBindUi(this));
		this.testoMsg.getElement().setAttribute("maxlength", "500");
		this.inviaMsgButton.getElement().setAttribute("style", "margin-top: 5px;");
		testoMsg.getElement().setAttribute("placeholder", "Scrivi un messaggio...");
		this.serviceImpl = reCallClientImpl;
	}

	// Inserisce un testo in labelok
	public void setLabelok(String text) {
		labelok.setText(text);
	}
	
	// Aggiunge un elemento alla lista di categorie selezionabili
	public void addCategoryToListInsertMessages(String category){
		this.categoryListBox.addItem(category);
	}
	
	// Pulisce la casella per l'inserimento dei messaggi
	private void clean(){
		testoMsg.setText("");
	}
	
	// Nasconde il widget di inserimento messaggi
	public void hide(){
		boxMessage.setVisible(false);
	}
	
	// Mostra il widget di inserimento messaggi
	public void show(){
		boxMessage.setVisible(true);
	}

	// Ottiene la categoria seleionata e dice al client di inserire il messaggio scritto nella
	// categoria selezionata. Controlla se il campo del messaggio è stato riempito.
	@UiHandler("inviaMsgButton")
	void onMsgButtonClick(ClickEvent event) {
		int indexCategorySelected = categoryListBox.getSelectedIndex();
		String cat = categoryListBox.getItemText(indexCategorySelected);
		if(!testoMsg.getText().equals("")){
			serviceImpl.insertMessage(cat, this.serviceImpl.retrieveLoggedUser(), testoMsg.getText());
			clean();
		}
		else{
			labelok.getElement().getStyle().setColor("red");
			labelok.setText("Riempire tutti i campi vuoti!");
		}
	}

	// Pulisce la lista delle categorie
	public void cleanSelCat(){
		categoryListBox.clear();
	}
}
