package it.recall.vssn.client.service;

import it.recall.vssn.client.widgets.AdminPanel;
import it.recall.vssn.client.widgets.Filters;
import it.recall.vssn.client.widgets.InsertMessages;
import it.recall.vssn.client.widgets.Login;
import it.recall.vssn.client.widgets.MessageBoard;
import it.recall.vssn.client.widgets.Navbar;
import it.recall.vssn.client.widgets.Registrazione;
import it.recall.vssn.client.widgets.SideMenu;
import it.recall.vssn.shared.Msg;
import it.recall.vssn.shared.Utente;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public class ReCallClientImpl implements ReCallClientInt {
	
	// Servizio asincrono per attendere le risposte dal server
	private ReCallServiceAsync service;
	
	// Oggetti che contengono i vari componenti grafici (e le proprie funzionalità)
	// di ciascun elemento grafico del sistema che vengono inserite nella GUI
	private Registrazione usersGuiRegistrazione;
	private Navbar navbarGui;
	private InsertMessages insertMsgGui;
	private Login usersLoginGui;
	private MessageBoard msgBoardGui;
	private Filters filtersGui;
	private SideMenu sideMenuGui;
	private AdminPanel adminPanelGui;
	
	// Variabili che contengono utenti e categorie selezionate nei filtri
	String userFilterSelected, categoryFilterSelected;
	
	// Salva globalmente il nome utente loggato
	String usernameLogged;
	
	// Flag per settare se chi è loggato è un utente o un amministratore
	Boolean isUserLogged, isAdminLogged;
	
	// Costruttore della classe, crea gli elementi grafici e li inizializza,
	// crea la comunicazione con il server e inizializza il servizio.
	// Verifica inoltre se è presente un cookie nel browser dell'utente:
	// se si, "riapre" la sessione dell'utente o amministratore che era precedentemente
	// loggato e "ricrea" l'ambiente grafico per l'utente o per l'amministratore
	public ReCallClientImpl(String url){
		this.service = GWT.create(ReCallService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
		
		this.navbarGui = new Navbar(this);
		this.usersGuiRegistrazione = new Registrazione(this);
		this.insertMsgGui = new InsertMessages(this);
		this.msgBoardGui = new MessageBoard(this);
		this.usersLoginGui = new Login(this);
		this.filtersGui = new Filters(this);
		this.sideMenuGui = new SideMenu(this);
		this.adminPanelGui = new AdminPanel(this);
		
		userFilterSelected="";
		categoryFilterSelected="";
		isUserLogged=false;
		isAdminLogged=false;
		
		if(Cookies.getCookie("VSSN_usernameLogged")!=null){
			usernameLogged = Cookies.getCookie("VSSN_usernameLogged");
			addLeftColumn();
			if(Cookies.getCookie("VSSN_usernameLogged").equals("admin")){
				isAdminLogged=true;
				retrieveMessages(userFilterSelected,categoryFilterSelected);
				adminPanelGui.getElement().removeAttribute("style");
				insertMsgGui.getElement().setAttribute("style", "display:none");
				sideMenuGui.getElement().setAttribute("style", "display:none");
				sideMenuGui.getElement().setAttribute("style", "display:none");
				adminPanelGui.showAdminPanel();
			}
			else{
				isUserLogged=true;
				retrieveMessages(userFilterSelected,categoryFilterSelected);
				adminPanelGui.getElement().setAttribute("style", "display:none");
				insertMsgGui.getElement().removeAttribute("style");
				filtersGui.getElement().setAttribute("style", "margin-top: 20px;");
				sideMenuGui.getElement().removeAttribute("style");
				sideMenuGui.getElement().removeAttribute("style");
				sideMenuGui.showSideMenu();
			}
		}
		else{
			adminPanelGui.getElement().setAttribute("style", "display:none");
			insertMsgGui.getElement().setAttribute("style", "display:none");
			sideMenuGui.getElement().setAttribute("style", "display:none");
		}
	}
	
	// Restituisco la gui contenente il modulo di login
	public Login getGUILogin(){
		return this.usersLoginGui;
	}
	
	// Restituisco la side menu
	public SideMenu getSideMenuGUI(){
		return this.sideMenuGui;
	}
	
	// Restituisco la side menu pulita
	public SideMenu getCleanedSideMenuGUI(){
		sideMenuGui.cleanFollowingListPanel();
		return this.sideMenuGui;
	}
	
	// Restituisco la gui contenente il modulo di registrazione
	public Registrazione getGUIRegistrazione(){
		return this.usersGuiRegistrazione;
	}
	
	// Restituisco una navbar diversa se un utente è loggato o meno
	// (se loggato mostro il pulsante di logout e il proprio nickname,
	// se non loggato, mostro i pulsanti di login e registrazione)
	public Navbar getGUINavbar(){
		if(Cookies.getCookie("VSSN_usernameLogged")!=null){
			this.navbarGui.isLogged();
		}
		else{
			this.navbarGui.isNotLogged();
		}
		return this.navbarGui;
		
	}
	
	// Restituisco il widget che permette l'inserimento dei messaggi
	public InsertMessages getInsertMsgGUI(){
		return this.insertMsgGui;
	}
	
	// Restituisco la sezione filtri con le categorie nel sistema
	// e se è loggato un utente riempio l'altro filtro con i suoi followers
	public Filters getFiltersGUI(){
		retrieveFollowingUsers(usernameLogged);
		retrieveCategories();
		return this.filtersGui;
	}
	
	// Restituisco la sezione followers dei filtri pulita (senza niente
	// al suo interno)
	public Filters getCleanedFiltersGUI(){
		filtersGui.cleanLstBoxFollowing();
		return this.filtersGui;
	}
	
	// Restituisco la message board pulita (senza messaggi al suo interno)
	public MessageBoard getCleanedMsgBoardGUI(){
		msgBoardGui.cleanMsgBoard();
		return this.msgBoardGui;
	}
	
	// Restituisco la message board popolata di messaggi (alla situazione
	// iniziale senza filtri) - il metodo è richiamato al solo primo avvio della
	// pagine del sistema
	public MessageBoard getMsgBoardGUI(){
		retrieveMessages("",""); //popolo la messageBoard coi messaggi
		return this.msgBoardGui;
	}
	
	// Restituisco la sidemenu quando devo mostrarla per un utente
	public SideMenu getFollowersGui(){
		return this.sideMenuGui;
	}
	
	// Restituisco la sidemenu pulita
	public SideMenu getCleanedBoard(){
		sideMenuGui.cleanResultPanel();
		return this.sideMenuGui;
	}

	// Restituisco il pannello amministratore da mostrare nella
	// colonna laterale
	public AdminPanel getAdminPanelGui(){
		return this.adminPanelGui;
	}
	
	// Restituisco il pannello amministratore pulito da mostrare nella
	// colonna laterale
	public AdminPanel getCleanedAdminPanelGui(){
		adminPanelGui.cleanAdminPanel();
		return this.adminPanelGui;
	}
	
	// Restituisco la lista filtri categorie pulita
	public Filters getCleanedCategoryFilterGui(){
		filtersGui.cleanLstBoxCategory();
		return this.filtersGui;
	}
	
	// Restituisco la lista filtri categorie del pannello amministratore pulita 
	public AdminPanel getCleanedCatLstBox() {
		adminPanelGui.clean();
		return this.adminPanelGui;
	}
	
	// Restituisco la lista filtri categorie del pannello inserimento messaggio pulita
	public InsertMessages getCleanedSelCat(){
		insertMsgGui.cleanSelCat();
		return this.insertMsgGui;
	}
	
	// Nascondo la side menu utente
	public SideMenu getCleanedSideMenuGui(){
		this.sideMenuGui.cleanFollowingListPanel();
		this.sideMenuGui.cleanResultPanel();
		this.sideMenuGui.getElement().setAttribute("style", "display:none");
		return this.sideMenuGui;
		
	}
	
	// Inserimento utente
	
	@Override
	public void insertUser(Utente ut) {
		this.service.insertUser(ut, new InsertUserCallback());
	}
	
	private class InsertUserCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se l'utente non è già presente nel db, e quindi è stato effettivamente registrato
		// nel db: setto il cookie nel client, nascondo il modulo di registrazione, cambio la navbar
		// e la mostra per un utente registrato, mostro il pannello di inserimento messaggi, e pulisco
		// il modulo di registrazione nascosto; altrimenti mostro un messaggio di errore.
		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>){
				ArrayList<String> response = (ArrayList<String>) result;
				if(response.get(0).equals("true")){
					setLoginCookie((String)response.get(1));
					usersGuiRegistrazione.hideMe();
					navbarGui.isLogged();
					insertMsgGui.show();
					usersGuiRegistrazione.cleanModule();
				}
				else {
					usersGuiRegistrazione.setLabelok("Nome utente già assegnato", "rosso");
				}
			}
			else{
				Window.alert("Errore");
			}
		}	
	}
	
	// Inserimento messaggio
	public void insertMessage(String categoria, String mittente, String msg) {
		this.service.insertMessage(categoria, mittente, msg, new InsertMessageCallback());
	}
	
	private class InsertMessageCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se il messaggio è stato inserito con successo senza errori da parte del server
		// (i messaggi di norma vengono sempre inseriti), richiedo una message board pulita
		// e la ripopolo di messaggi eventualmente con i filtri settati in precedenza
		// (aggiorno quindi la message board)
		@Override
		public void onSuccess(Object result) {
			getCleanedMsgBoardGUI();	//pulisco la messageBoard
			retrieveMessages(userFilterSelected,categoryFilterSelected); //aggiungo messaggi alla messageBoard
		
		}
	}
	
	// Ottengo la lista dei following di un utente se l'utente è loggato o non è l'admin
	// altrimenti mostro il filtro dei following disabilitato
	@Override
	public void retrieveFollowingUsers(String usernameLogged) {
		if(usernameLogged!=null && !usernameLogged.equals("admin"))
			this.service.retrieveFollowingUsers(usernameLogged, new RetrieveFollowingUsersCallback());
		else
			filtersGui.setUtentiFilterLabel("Nessun following selezionabile", "disabled");
	}
	
	private class RetrieveFollowingUsersCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se la lista dei following ottenuta non è vuota, o se non vengono lanciati errori
		// nel retrieve dei following, viene abilitata la lista con i filtri dei following
		// e vengono inseriti i following nella lista, altrimenti mostro una lista disabilitata
		// con un messaggio.
		@Override
		public void onSuccess(Object result) {
			ArrayList<String> listaUtenti = (ArrayList<String>) result;
			if(!listaUtenti.isEmpty()){
				if(!listaUtenti.get(0).equals("@@ERRORE@@")){
					filtersGui.enable();
					filtersGui.addUserToList("Tutti gli utenti");
					for (String u : listaUtenti){
						filtersGui.addUserToList(u);
						sideMenuGui.showFollowing(u);
					}
				}
				else{
					filtersGui.setUtentiFilterLabel("Nessun following selezionabile", "disabled");
					removeLoginCookie();
				}
			}
			else {
				filtersGui.setUtentiFilterLabel("Nessun following selezionabile", "disabled");
			}
		}	
	}
	
	// Setto il filtro delle categorie e ricreo la lista messaggi con
	// le categorie e i following selezionati (cioè, mostro la lista aggiornata
	// dei messaggi in base alle categorie e following selezionati)
	public void filterCategory(String category){
		categoryFilterSelected=category;
		retrieveMessages(userFilterSelected,categoryFilterSelected);
	}
	
	// Setto il filtro dei following e ricreo la lista messaggi con
	// le categorie e i following selezionati (cioè, mostro la lista aggiornata
	// dei messaggi in base alle categorie e following selezionati)
	public void filterUser(String user){
		userFilterSelected=user;
		retrieveMessages(userFilterSelected,categoryFilterSelected);
	}
	
	// Ottengo la lista delle categorie presenti nel DB
	@Override
	public void retrieveCategories() {
		this.service.retrieveCategories(new RetrieveCategoriesCallback());
	}
	
	private class RetrieveCategoriesCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se ottengo una lista di categorie vuota, mostro una casella disabilitata
		// altrimenti popolo la lista di categorie disponibili nel pannello amministratore
		// nel pannello messaggi e nella sezione dei filtri e la abilito
		@Override
		public void onSuccess(Object result) {
			HashMap<String,String> listaCategorie = (HashMap<String,String>) result;
			if(!listaCategorie.isEmpty()){
				filtersGui.addCategoryToList("Tutte le categorie", "");
				filtersGui.enableCategorie();
				for (Entry<String, String> c : listaCategorie.entrySet()){
					filtersGui.addCategoryToList(c.getKey().toString(), c.getValue().toString());
					insertMsgGui.addCategoryToListInsertMessages(c.getKey().toString());
					adminPanelGui.addCategoryToListAdmin(c.getKey().toString());
				}
			}
			else {
				filtersGui.setCategorieFilterLabel("Nessun filtro presente nel DB", "disabled");
			}
		}	
	}
	
	// Ottengo la lista dei messaggi presenti nel DB
	@Override
	public void retrieveMessages(String user, String category) {
		this.service.fillMessageBoard(user, category, new fillMessageBoardCallback());
	}
	
	private class fillMessageBoardCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Pulisco la message board, e inserisco i messaggi presenti nel DB nella message board
		// (viene mostrata una diversa message board se è loggato un utente o un admin)
		@Override
		public void onSuccess(Object result) {
			msgBoardGui.cleanMsgBoard();
			ArrayList<Msg> message = (ArrayList<Msg>) result;
			Collections.reverse(message);
			if (!message.isEmpty()) {
				for (Msg s : message){
					msgBoardGui.addMsgToMsgBoard(s,isUserLogged,isAdminLogged);
				}
			}
		}
	}
	
	// Effettuo il login
	@Override
	public void login(String username,String password) {
		this.service.login(username,password, new LoginCallback());
	}
	
	private class LoginCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se la risposta che ricevo al login è di loginOk, setto il cookie
		// sul client, avvio tutto i servizi collegati, nascondo il modulo
		// di login e lo pulisco.
		// Se le risposta di password o username sono sbagliate o se i campi non sono
		// sbagliati, mostro un messaggio di errore.
		@Override
		public void onSuccess(Object result) {
			if (result instanceof ArrayList<?>) {
				ArrayList<String> response = (ArrayList<String>) result;
				
				String reply = response.get(0);
				switch (reply) {
				case "loginOk":
					setLoginCookie(response.get(1));
					usersLoginGui.hide();
					usersLoginGui.cleanLogin();
					break;
				case "passwdWrong":
					usersLoginGui.setLabelok("Password errata");
					usersLoginGui.getLabelok().getElement().getStyle().setColor("red");
					usersLoginGui.getPass().setFocus(true);
					usersLoginGui.getPass().selectAll();
					break;
				case "usernameWrong":
					usersLoginGui.setLabelok("Utente errato");
					usersLoginGui.getLabelok().getElement().getStyle().setColor("red");
					usersLoginGui.getUser().setFocus(true);
					usersLoginGui.getUser().selectAll();
					break;
				case "missing":
					usersLoginGui.setLabelok("Riempire tutti gli spazi");
					usersLoginGui.getLabelok().getElement().getStyle().setColor("red");
					break;
				default:
					break;
				}
			}
		}
	}

	// Cerco un utente da parte di un utente loggato
	@Override
	public void searching(String user2search, String username) {
		this.service.searching(user2search, username, new searchingCallback());
	}
	
	private class searchingCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Inserisco come risultati della ricerca la lista di nomi tornati dalla ricerca
		// quando non sono lo username loggato o l'admin, altrimenti mostro
		// un messaggio
		@Override
		public void onSuccess(Object result) {
			ArrayList<String> userList = (ArrayList<String>) result;
			if (!userList.isEmpty()) {
				getCleanedBoard();
				for (String user : userList){
					String[] parts = user.split("-");
					String userName = parts[0];
					if(!userName.equals(usernameLogged) && !userName.equals("admin") ){
						sideMenuGui.showSearchResults(user);
					}
				}
			}
			else{
				getCleanedBoard();
				sideMenuGui.showSearchResults("NESSUN RISULTATO TROVATO!");
				}
		}
	}
	
	// Aggiungo un following ad un utente
	@Override
	public void addFollowing(String username2Follow, String username) {
		this.service.addFollowing(username2Follow, username, new addFollowingCallback());
	}
	
	private class addFollowingCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se l'operazione va a buon fine, pulisco le liste dei following
		// sia nei filtri che nella barra laterale, e ricarico le liste dei following
		@Override
		public void onSuccess(Object result) {
			getCleanedFiltersGUI();
			getCleanedSideMenuGUI();
			retrieveFollowingUsers(usernameLogged);
		}	
	}

	// Rimuovo un following di un utente
	@Override
	public void removeFollowing(String username2Follow, String username) {
		this.service.removeFollowing(username2Follow, username, new removeFollowingCallback());
	}
	
	private class removeFollowingCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se l'operazione va a buon fine, pulisco le liste dei following
		// sia nei filtri che nella barra laterale, e ricarico le liste dei following
		@Override
		public void onSuccess(Object result) {
			getCleanedFiltersGUI();
			getCleanedSideMenuGUI();
			retrieveFollowingUsers(usernameLogged);
		}	
	}
	
	// Setto il cookie di durata 1 giorno quando un utente si logga o si registra, 
	// cambio i pulsanti mostrati nella navbar, setto lo username loggato nel client,
	// setto l'utente loggato nella classe della sidemenu con la ricerca e i following,
	// ricarico le liste dei following.
	// Se si logga un utente (non admin), visualizzo nella GUI il pannello di inserimento
	// messaggi, setto che è stato loggato un utente, ricarico la message board e mostro la sidemenu.
	// Se si loggato l'admin, nascondo il pannello di inserimento messaggi e la side menu
	// setto che si è loggato l'admin, ricarico la message board, mostro il pannello amministratore.
	public void setLoginCookie(String username){
		final long DURATION = 1000 * 60 * 60 * 24 * 1;
		Date expires = new Date(System.currentTimeMillis() + DURATION);
		Cookies.setCookie("VSSN_usernameLogged", username, expires, null, "/", false);
		navbarGui.isLogged();
		this.usernameLogged = username;
		
		this.sideMenuGui.setUsernameLogged(this.usernameLogged);
		
		sideMenuGui.cleanFollowingListPanel();
		filtersGui.cleanLstBoxFollowing();
		retrieveFollowingUsers(username);
		addLeftColumn();
		
		if(!username.equals("admin")){	//utente generico loggato
			insertMsgGui.show();
			filtersGui.getElement().setAttribute("style", "margin-top: 20px;");
			sideMenuGui.getElement().removeAttribute("style");
			adminPanelGui.getElement().setAttribute("style", "display:none");
			isUserLogged=true;
			retrieveMessages(userFilterSelected,categoryFilterSelected);
			adminPanelGui.getElement().setAttribute("style", "display:none");
			insertMsgGui.getElement().removeAttribute("style");
			sideMenuGui.showSideMenu();
		}
		else{ //utente Admin Loggato
			filtersGui.getElement().setAttribute("style", "margin-top: 0px;");
			insertMsgGui.hide();
			sideMenuGui.getElement().setAttribute("style", "display:none");
			adminPanelGui.getElement().removeAttribute("style");
			isAdminLogged=true;
			retrieveMessages(userFilterSelected,categoryFilterSelected);
			adminPanelGui.getElement().removeAttribute("style");
			insertMsgGui.getElement().setAttribute("style", "display:none");
			adminPanelGui.showAdminPanel();

		}
	}
	
	// Rimuovo il cookie al logout, nascondo eventuali pannelli laterali aperti,
	// ricarico la message board, rimuovo la colonna sinistra e setto a false
	// i flag del tipo di utente loggato.
	public void removeLoginCookie(){
		Cookies.removeCookie("VSSN_usernameLogged", "/");
		navbarGui.isNotLogged();
		insertMsgGui.hide();
		usernameLogged = "";
		userFilterSelected="";
		
		filtersGui.getElement().setAttribute("style", "margin-top: 0px;");
		getCleanedSideMenuGui();
		filtersGui.cleanLstBoxFollowing();
		adminPanelGui.cleanLabels();
		retrieveFollowingUsers(null);
		adminPanelGui.getElement().setAttribute("style", "display:none");
		insertMsgGui.getElement().setAttribute("style", "display:none");
		removeLeftColumn();
		
		isUserLogged=false;
		isAdminLogged=false;
		retrieveMessages(userFilterSelected,categoryFilterSelected);
	}
	
	// Ritorna lo username dell'utente loggato
	public String retrieveLoggedUser(){
		return usernameLogged;
	}

	// Viene inserito nel db l'utente admin se non presente.
	@Override
	public void manageAdministrator() {
		this.service.manageAdministrator(new manageAdministratorCallback());
	}
	
	private class manageAdministratorCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onSuccess(Object result) {
			// TODO Auto-generated method stub
		}	
	}

	// Inserisco una categoria nel sistema
	@Override
	public void insertCategory(String newCategory) {
		this.service.insertCategory(newCategory, new insertCategoryCallback());
	}
	
	private class insertCategoryCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Verifico se la categoria è stata inserita, se si mostro un messaggio
		// di aggiunta e ricarico le lista delle categorie, altrimenti mostro
		// un messaggio di errore
		@Override
		public void onSuccess(Object result) {
			Boolean inserted = (Boolean) result;
			
			adminPanelGui.showResultInsertionOperationAdmin(inserted);
			if(inserted){
				getCleanedCategoryFilterGui(); 
				getCleanedCatLstBox();
				getFiltersGUI();
				getAdminPanelGui();
				getCleanedSelCat();
			}
		}	
	}

	// Elimino una categoria
	@Override
	public void deleteCategory(String delCategory) {

		this.service.deleteCategory(delCategory, new deleteCategoryCallback());
	}

	private class deleteCategoryCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se la categoria è stata eliminata, mostro un messaggio di avvenuta
		// cancellazione e ricarico le liste, altrimenti mostro un messaggio di errore
		@Override
		public void onSuccess(Object result) {
			Boolean deleted = (Boolean) result;

			adminPanelGui.showResultDeleteOperationAdmin(deleted);
			if(deleted){
				getCleanedCategoryFilterGui();
				getCleanedCatLstBox();
				getFiltersGUI();
				getAdminPanelGui();
				getCleanedSelCat();
			}
		}	

	}
	
	// Elimino un messaggio
	@Override
	public void deleteMessage(int cnt) {
		this.service.deleteMessage(cnt, new deleteMessageCallback());
	}
	
	private class deleteMessageCallback implements AsyncCallback{

		@Override
		public void onFailure(Throwable caught) {
			// TODO Auto-generated method stub
		}

		// Se eliminato, ricarico la message board
		@Override
		public void onSuccess(Object result) {
			Boolean deleted = (Boolean) result;
			
			if(deleted){
				retrieveMessages(userFilterSelected,categoryFilterSelected);
			}
		}	
	}
	
	// Mostro la colonna di sinistra
	private void addLeftColumn(){
		Document.get().getElementById("leftColumn").setAttribute("class", "col-md-3 col-md-offset-1");
		Document.get().getElementById("rightColumn").setAttribute("class", "col-md-6 col-md-offset-1");
	}
	
	// Rimuovo la colonna di sinistra
	private void removeLeftColumn(){
		Document.get().getElementById("leftColumn").setAttribute("class", "");
		Document.get().getElementById("rightColumn").setAttribute("class", "col-md-6 col-md-offset-3");
	}
	
}