package it.recall.vssn.client.service;

import java.util.ArrayList;
import java.util.HashMap;

import it.recall.vssn.client.widgets.Message;
import it.recall.vssn.shared.Msg;
import it.recall.vssn.shared.Utente;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/*
 * Client-side stub for users operations
 */

@RemoteServiceRelativePath("vssn_recall")
public interface ReCallService extends RemoteService {
	ArrayList<String> insertUser(Utente ut);
	String insertMessage(String categoria, String mittente, String msg);
	ArrayList<String> retrieveFollowingUsers(String usernameLogged);
	HashMap<String,String> retrieveCategories();
	ArrayList<Msg> fillMessageBoard(String user, String category);
	ArrayList<String> login(String username,String password);
	ArrayList<String> searching(String user2search, String username);
	void addFollowing(String username2Follow, String username);
	void removeFollowing(String username2Follow, String username);
	void manageAdministrator();
	boolean insertCategory(String newCategory);
	boolean deleteMessage(int cnt);
	boolean deleteCategory(String delCategory);
}
