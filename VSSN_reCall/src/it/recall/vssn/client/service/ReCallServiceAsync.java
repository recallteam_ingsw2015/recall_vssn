package it.recall.vssn.client.service;

import it.recall.vssn.shared.Utente;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ReCallServiceAsync {

	void insertUser(Utente ut, AsyncCallback callback) throws IllegalArgumentException;
	void login(String username,String password, AsyncCallback callback) throws IllegalArgumentException;
	void insertMessage(String categoria, String mittente, String msg, AsyncCallback callback) throws IllegalArgumentException;
	void fillMessageBoard(String user, String category, AsyncCallback callback) throws IllegalArgumentException;
	void retrieveFollowingUsers(String usernameLogged, AsyncCallback callback) throws IllegalArgumentException;
	void retrieveCategories(AsyncCallback callback) throws IllegalArgumentException;
	void searching(String user2search, String username, AsyncCallback callback) throws IllegalArgumentException;
	void addFollowing(String username2Follow, String username, AsyncCallback callback) throws IllegalArgumentException;
	void removeFollowing(String username2Follow, String username, AsyncCallback callback) throws IllegalArgumentException;
	void manageAdministrator(AsyncCallback callback) throws IllegalArgumentException;
	void insertCategory(String newCategory, AsyncCallback callback) throws IllegalArgumentException;
	void deleteMessage(int cnt, AsyncCallback callback) throws IllegalArgumentException;
	void deleteCategory(String delCategory, AsyncCallback callback) throws IllegalArgumentException;
}	
