package it.recall.vssn.client.service;

import it.recall.vssn.shared.Utente;

public interface ReCallClientInt {

	void insertUser(Utente ut);
	void insertMessage(String categoria, String mittente, String msg);
	void retrieveFollowingUsers(String usernameLogged);
	void retrieveCategories();
	void retrieveMessages(String user, String category);
	void login(String username,String password);
	void searching(String user2search, String username);
	void addFollowing(String username2Follow, String username);
	void removeFollowing(String username2Follow, String username);
	void manageAdministrator();
	void insertCategory(String newCategory);
	void deleteMessage(int cnt);
	void deleteCategory(String delCategory);
}