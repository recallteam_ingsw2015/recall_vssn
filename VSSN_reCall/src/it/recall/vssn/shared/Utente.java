package it.recall.vssn.shared;

import java.io.Serializable;
import java.util.ArrayList;

// Classe che identifica un utente del sistema

public class Utente implements Serializable {

    /**
* Generato serialVersionUID
*/
private static final long serialVersionUID = -294536685685295944L;
private String nome;
    private String citta;
    private String dataNascita;
    private char sesso;
    private String username;
    private String password;
    private String email;
    private ArrayList<String> followingList;

    public Utente(String nome, String citta, String dataN, char s, String usern, String pass){
    	this.nome=nome;
        this.citta=citta;
        this.dataNascita=dataN;
        this.sesso=s;
        this.username=usern;
        this.password=pass;
        this.email=null;
        this.followingList = new ArrayList<String>();
    }

    public Utente(String nome, String citta, String dataN, char s, String usern, String pass, String email){
    	this.nome=nome;
        this.citta=citta;
        this.dataNascita=dataN;
        this.sesso=s;
        this.username=usern;
        this.password=pass;
        this.email=email;
        this.followingList = new ArrayList<String>();
    }
    
    public Utente(){
    }
    
    public void addFollowingList(String following){
        this.followingList.add(following);
    }
    
	public void removeFollowingList(String following) {
		for(int i = 0; i < this.followingList.size(); i++) {
			if(this.followingList.get(i).equals(following)){
				this.followingList.remove(i);
				break;	// non faccio scorrere tutta quanta la lista
			}
		}
	}
    
	public boolean checkFollowerList(String following) {
		boolean check = false;

		if (this.followingList.contains(following)) {
			check = true;
			return check;
		} else {
			return check;
		}
	}

	public ArrayList<String> getFollowerList(){
		return this.followingList;
	}
	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String getName() {
		return this.nome;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
    @Override
    public String toString(){
        String result = "Nome: " + this.nome + "\nCitta: " + this.citta + "\nData di Nascita: " + this.dataNascita + "\nSesso: " + this.sesso + "\nUsername: " + this.username + "\nPassword: " + this.password + "\nEmail: " + this.email + "\nFollowing: ";
        for (String key : followingList ) {
            result += "" + key + ", ";
        }
        return result;
    }

}
