package it.recall.vssn.shared;

import java.io.Serializable;

// Classe che identifica un messaggio del sistema

public class Msg implements Serializable{
	
	private String text;
	private String category;
	private String user;
	private String timestamp;
	private Integer id;
	
	public Msg(){
	}
	
	public Msg(String text, String category, String user, String timestamp, Integer id){
		this.text = text;
		this.category = category;
		this.user = user;
		this.id = id;
		this.setTimestamp(timestamp);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getId(){
		return this.id;
	}
	
	@Override
	public String toString(){
		String r =  this.category+ " - "+ this.user +" - "+this.text;
		return r;
	}

}
