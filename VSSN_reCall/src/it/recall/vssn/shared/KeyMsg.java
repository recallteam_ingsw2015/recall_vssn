package it.recall.vssn.shared;

import java.io.Serializable;

// Classe che identifica una categoria/chiave di un messaggio del sistema

@SuppressWarnings("serial")
public class KeyMsg implements Comparable<KeyMsg>,Serializable {

	private String categoria;
	private String mittente;
	private Integer id;
	
	public KeyMsg(){
		
	}
	
	public KeyMsg (String categoria, String mittente){
		this.categoria = categoria;
		this.mittente = mittente;
	}
	
	public KeyMsg (String categoria, String mittente, Integer id){
		this.categoria = categoria;
		this.mittente = mittente;
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}
	
	public String getCategoria(){
		return this.categoria;
	}
	
	public String getMittente(){
		return this.mittente;
	}

	@Override
	public String toString(){
		String r =  this.categoria+ " "+ this.mittente + " "+this.id;
		return r;
	}

	@Override
	public int compareTo(KeyMsg other) {
		return this.id.compareTo(other.id);
	}
}
